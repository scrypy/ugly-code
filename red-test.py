#turn "My grandma is sweet" into "The wolf ate grandma"

orig = 'My grandma is sweet'
new = 'The wolf ate grandma'
print('len-orig:'+str(len(orig+'a'))+'--len-new:'+str(len(new)))
print('orig:'+str([ord(l) for l in orig+'a']))
print('new:'+str([ord(l) for l in new]))
#shifts = [(y-x) for x,y in zip([ord(l) for l in orig+'a'],[ord(l) for l in new])]
shifts = [7, -17, 69, -71, 5, 14, -2, 2, -77, 0, 84, -4, -83, 71, -1, -22, 9, -1, -7, 0] 
print(str(shifts))
#print('diff:'+str([(y-x) for x,y in zip([ord(l) for l in orig],[ord(l) for l in new])]))
# orig:[77, 121, 32, 103, 114, 97, 110, 100, 109, 97, 32, 105, 115, 32,  115, 119, 101, 101, 116]
# new: [84, 104, 101, 32, 119, 111,108, 102, 32,  97, 116,101, 32,  103, 114, 97,  110, 100, 109, 97]
#shifts=[7,  -17,  69, -71,   5,  14,-2, 2,  -77,0, 84, -4,-83,  71,-1,-22, 9, -1,-7]
#shifts = [-7, 17, -69, 71,   -5, -14, 2, -2, 77, 0, -84, 4, 83, -71, 1, 22, -9, 1, 7]
#shifts = [x for x in range(len(orig))]
print([chr(ord(y)+x) for x,y in zip(shifts,[l for l in orig+'a'])])
